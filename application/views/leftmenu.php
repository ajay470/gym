<?php $this->load->helper('header_helper');

$admin = admin_details();

?>
<div class="leftpanel">
    <div class="media profile-left">
        <a class="pull-left profile-thumb" href="profile.html">
            <img class="img-circle" src="images/photos/profile.png" alt="">
        </a>
        <div class="media-body">
            <h4 class="media-heading"><?= $admin['first_name'].' '.$admin['last_name'] ?></h4>
            <small class="text-muted">Beach Lover</small>
        </div>
    </div><!-- media -->

    <h5 class="leftpanel-title">Navigation</h5>
    <ul class="nav nav-pills nav-stacked">
        <li><a href="<?= base_url('dashboard'); ?>"><i class="fa fa-home"></i> <span>Dashboard</span></a></li>
<!--        <li><a href="messages.html"><span class="pull-right badge">5</span><i class="fa fa-envelope-o"></i> <span>Messages</span></a></li>-->
        <li class="parent"><a href=""><i class="fa fa-suitcase"></i> <span>Clients</span></a>
            <ul class="children">
                <li><a href="">View Clients</a></li>
                <li><a href="">Add New Client</a></li>
            </ul>
        </li>
        <li class="parent"><a href=""><i class="fa fa-suitcase"></i> <span>Trainers</span></a>
            <ul class="children">
                <li><a href="">View Trainers</a></li>
                <li><a href="">Add New Trainer</a></li>
            </ul>
        </li>

    </ul>

</div>