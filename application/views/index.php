<?php
$this->load->view('header');
?>

<section>
    <div class="mainwrapper">

        <?php $this->load->view('leftmenu'); ?>

        <div class="mainpanel">
            <div class="pageheader">
                <div class="media">
                    <div class="pageicon pull-left">
                        <i class="fa fa-home"></i>
                    </div>
                    <div class="media-body">
                        <ul class="breadcrumb">
                            <li><a href=""><i class="glyphicon glyphicon-home"></i></a></li>
                            <li><a href="">Pages</a></li>
                            <li>Blank Page</li>
                        </ul>
                        <h4>Blank Page</h4>
                    </div>
                </div><!-- media -->
            </div><!-- pageheader -->

            <div class="contentpanel">


                <!-- CONTENT GOES HERE -->

            </div><!-- contentpanel -->

        </div>
    </div><!-- mainwrapper -->
</section>

<?php $this->load->view('footer'); ?>