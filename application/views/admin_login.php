<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>themelock.com - Chain Responsive Bootstrap3 Admin</title>

    <link href="<?= base_url('public'); ?>/css/style.default.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="<?= base_url('public/'); ?>js/html5shiv.js"></script>
    <script src="<?= base_url('public/'); ?>js/respond.min.js"></script>
    <![endif]-->
</head>

<body class="signin">


<section>

    <div class="panel panel-signin">
        <div class="panel-body">
            <div class="logo text-center">
                <img src="<?= base_url('public'); ?>/images/logo-primary.png" alt="Chain Logo" >
            </div>
            <br />
<!--            <h4 class="text-center mb5">Already a Member?</h4>-->
            <p class="text-center">Sign in to your account</p>

            <div class="mb30"></div>

            <form id="login_form" action="" method="post">
                <div class="input-group mb15">
                    <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                    <input name="username" required type="text" class="form-control" placeholder="Username">
                </div><!-- input-group -->
                <div class="input-group mb15">
                    <span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
                    <input name="password" required type="password" class="form-control" placeholder="Password">
                </div><!-- input-group -->

                <div class="clearfix">
                    <div class="pull-left">
                        <div class="ckbox ckbox-primary mt10">
                            <input type="checkbox" id="rememberMe" value="1">
                            <label for="rememberMe">Remember Me</label>
                        </div>
                    </div>
                    <div class="pull-right">
                        <button type="submit" class="btn btn-success">Sign In <i class="fa fa-angle-right ml5"></i></button>
                    </div>
                </div>
            </form>

        </div><!-- panel-body -->
        <div class="panel-footer">
            <p id="error"></p>
<!--            <a href="signup" class="btn btn-primary btn-block">Not yet a Member? Create Account Now</a>-->
        </div><!-- panel-footer -->
    </div><!-- panel -->

</section>

<?php $this->load->view('footer'); ?>

<script>

    $('#login_form').on('submit',function(e){

        e.preventDefault();
        if($('#login_form').valid())
        {
            $.ajax({
                type:"POST",
                url:"<?= base_url('home') ?>/login",
                data:$('#login_form').serialize(),
                success:function(result)
                {
                    if(result == 'success')
                    {
                        window.location.href = "<?php echo base_url('dashboard'); ?>"
                    }
                    else
                    {
                        $('#error').html(result);
                    }

                }
            })
        }


    });

</script>
