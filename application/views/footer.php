
<script src="<?=base_url('public') ?>/js/jquery-1.11.1.min.js"></script>
<script src="<?=base_url('public') ?>/js/jquery-migrate-1.2.1.min.js"></script>
<script src="<?=base_url('public') ?>/js/bootstrap.min.js"></script>
<script src="<?=base_url('public') ?>/js/modernizr.min.js"></script>
<script src="<?=base_url('public') ?>/js/pace.min.js"></script>
<script src="<?=base_url('public') ?>/js/retina.min.js"></script>
<script src="<?=base_url('public') ?>/js/jquery.cookies.js"></script>
<script src="<?=base_url('public') ?>/js/jquery.validate.min.js"></script>
<script src="<?=base_url('public') ?>/js/custom.js"></script>

</body>
</html>