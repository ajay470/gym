<?php

class Homemodel extends CI_Model
{
    public function admin_login($data)
    {

        $check = $this->db->select('*')
            ->from('gym_owners')
            ->where('username',$data['username'])
            ->or_where('email',$data['username'])
            ->where('password',md5($data['password']))
            ->where('status','1')
            ->get()
            ->row_array();

        if($check)
        {
            return $check;
        }
        else
        {
            return false;
        }

    }

    public function admin_details($id)
    {
        return $this->db->get_where('gym_owners',array('id'=>$id))->row_array();
    }
}