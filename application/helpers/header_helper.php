<?php


function admin_details()
{
    $ci = &get_instance();
    $ci->load->model('homemodel','home');
    $id = $ci->session->userdata('admin_id');
    $data = array();
    if($id > 0) {
        $data = $ci->home->admin_details($id);
    }
    else
    {
        $ci->session->set_userdata(array('admin_login'=>false));
        redirect(base_url());
    }
    return $data;
}