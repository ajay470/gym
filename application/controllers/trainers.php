<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Trainers extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();

        if (!$this->session->userdata('admin_login')) {
            redirect(base_url());
        }

        $this->load->model('homemodel', 'home');
    }

    public function index()
    {
        $this->load->view('trainers_list');
    }


    public function add()
    {
        $this->load->view('add_new_trainer');
    }

}
