<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Clients extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();

        if (!$this->session->userdata('admin_login')) {
            redirect(base_url());
        }

        $this->load->model('homemodel', 'home');
    }

    public function index()
    {
        $this->load->view('clients_list');
    }


    public function add()
    {
        if($this->input->post())
        {
            $data = $this->input->post();
        }

        $this->load->view('add_new_client');
    }

}
