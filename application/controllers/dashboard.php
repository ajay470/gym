<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Dashboard extends CI_Controller {

    public function __construct()
    {
        parent::__construct();

        if(!$this->session->userdata('admin_login'))
        {
            redirect(base_url());
        }

        $this->load->model('homemodel','home');
    }


    public function index()
    {
        $id = $this->session->userdata('admin_id');
        $data = array();
        $abc = array();
        if($id > 0) {
           $data['admin'] = $this->home->admin_details($id);
        }
        else
        {
            $this->session->set_userdata(array('admin_login'=>false));
            redirect(base_url());
        }


        $this->load->view('dashboard',$data);
    }

    public function logout()
    {
        $this->session->set_userdata(array('admin_login'=>false,'admin_id'=>''));
        redirect(base_url());
    }

}