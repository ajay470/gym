<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('homemodel','home');
    }

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
        if($this->session->userdata('admin_login'))
        {
            redirect(base_url('dashboard'));
        }
		$this->load->view('admin_login');
	}

    public function login()
    {
        if ($this->input->is_ajax_request()) {

            $data = $this->input->post();
            if(array_key_exists('username',$data) && array_key_exists('password',$data)) {
               $check = $this->home->admin_login($data);

                if($check != false)
                {
                    $this->session->set_userdata(array('admin_login'=>true,'admin_id'=>$check['id']));

                    die('success');
                }
                else
                {
                    die('Wrong Username Or Password');
                }
            }
            else
            {
                die('Something went wrong');
            }
        }
        else
        {
            exit('No direct script access allowed');
        }
    }
}

/* End of file home.php */
/* Location: ./application/controllers/home.php */